# noobChat聊天室（完善中）


### 介绍

一个集听歌聊天等一体的在线聊天室，支持多房间和创建私人房间，支持房间加密和切换房间模式。此仓库为APP端代码仓库。


### 使用说明

体验一下H5版本：<a href="https://chat.shabbb.com/" target="_blank">chat.shabbb.com</a>

APP暂不提供编译包或下载地址，请自行clone当前项目，导入到HbuilderX并编译运行项目体验。