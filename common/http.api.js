// 用户接口
let emailUrl = '/sms/email';
let loginUrl = '/user/login';
let getMyInfoUrl = '/user/getMyInfo';
let updateMyInfoUrl = '/user/updateMyInfo';
let getUserInfoUrl = '/user/getUserInfo';

// 图片接口
let uploadimageUrl = '/attach/uploadimage';

// 歌曲接口
let songSearchUrl = '/song/search';

// 房间接口
let getWebsocketUrl = '/room/getWebsocketUrl';
let getMessageListUrl = '/message/getMessageList';
let getRoomInfoUrl = '/room/getRoomInfo'
let getHotRoomsUrl = '/room/hotRooms'

// 发送消息接口
let sendMessageUrl = '/message/send'
const install = (Vue, vm) => {
    // 用户接口
    let email = (params = {}) => vm.$u.post(emailUrl, params);
    let login = (params = {}) => vm.$u.post(loginUrl, params);
    let getMyInfo = (params = {}) => vm.$u.post(getMyInfoUrl, params);
    let updateMyInfo = (params = {}) => vm.$u.post(updateMyInfoUrl, params);
    let getUserInfo = (params = {}) => vm.$u.post(getUserInfoUrl, params);

    // 图片接口
    let uploadimage = (params = {}) => vm.$u.post(uploadimageUrl, params);

    // 歌曲接口
    let songSearch = (params = {}) => vm.$u.post(songSearchUrl, params);

    // 房间接口
    let getWebsocket = (params = {}) => vm.$u.post(getWebsocketUrl, params);
    let getMessageList = (params = {}) => vm.$u.post(getMessageListUrl, params);
    let getRoomInfo = (params = {}) => vm.$u.post(getRoomInfoUrl, params);
    let getHotRooms = (params = {}) => vm.$u.post(getHotRoomsUrl, params);
    
    // 发送消息接口
    let sendMessage = (params = {}) => vm.$u.post(sendMessageUrl, params);
    // 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
    vm.$u.api = {
        // 用户接口
        email,
        login,
        getMyInfo,
        updateMyInfo,
        getUserInfo,

        // 图片接口
        uploadimage,

        // 音乐接口
        songSearch,

        // 房间接口
        getWebsocket,
        getMessageList,
        getRoomInfo,
        getHotRooms,
        sendMessage
    };
}

export default {
    install
}
